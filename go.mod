module github.com/go-mysql-crud

go 1.14

require (
	github.com/antonfisher/nested-logrus-formatter v1.0.3
	github.com/go-chi/chi v4.0.4+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.5.0
	github.com/urfave/cli/v2 v2.2.0
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	golang.org/x/sys v0.0.0-20200327173247-9dae0f8f5775 // indirect
)
